var exec = cordova.require('cordova/exec');

var SunmiK1Manager = function() {
    console.log('Iniciando SunmiK1Manager...');
}

SunmiK1Manager.prototype.printData = function(data, onSuccess, onError){
    var errorCallback = function(obj){
        onError(obj);
    };
    var successCallback = function(obj){
        onSuccess(obj);
    };
    exec(successCallback, errorCallback, 'SunmiK1Manager', 'printingService', [data]);
}

SunmiK1Manager.prototype.connectPrinter = function(data, onSuccess, onError){
    var errorCallback = function(obj){
        onError(obj);
    };
    var successCallback = function(obj){
        onSuccess(obj);
    };
    exec(successCallback, errorCallback, 'SunmiK1Manager', 'connectPrinter', []);
}

SunmiK1Manager.prototype.disconnectPrinter = function(data, onSuccess, onError){
    var errorCallback = function(obj){
        onError(obj);
    };
    var successCallback = function(obj){
        onSuccess(obj);
    };
    exec(successCallback, errorCallback, 'SunmiK1Manager', 'disconnectPrinter', []);
}