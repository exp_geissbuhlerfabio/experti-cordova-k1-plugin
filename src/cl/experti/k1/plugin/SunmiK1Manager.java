package cl.experti.k1.plugin;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;
import cl.experti.k1.plugin.ExtPrinterService;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/*
* Clase centralizada para construir plugin Ionic
* */

public class SunmiK1Manager extends CordovaPlugin {

    private Context context;
    private ExtPrinterService mPrinter = null;
    private String TAG = "SunmiK1Manager";
    private ServiceConnection connService = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mPrinter = null;
            Log.i(TAG, "==== onServiceDisconnected: el servicio de impresión se desconectó.");
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mPrinter = ExtPrinterService.Stub.asInterface(service);
            Log.i(TAG, "==== onServiceConnected: conectado al servicio de impresión.");
            try{mPrinter.printerInit();}
            catch (RemoteException e){
                Log.i(TAG, "onServiceConnected: No se pudo conectar el servicio de impresora.");
            }
        }
    };

    public SunmiK1Manager() {
        connectPrinter();
    }

    @Override
    public boolean execute(String action, JSONArray params, CallbackContext callbackContext) throws JSONException{
        if(checkPrinterStatus()){
            switch (action){
                case "connectPrinter":
                    connectPrinter();
                    callbackContext.success();
                    return true;
                case "disconnectPrinter":
                    disconnectPrinterService();
                    callbackContext.success();
                    return true;
                case "printData":
                    printingService(params);
                    callbackContext.success();
                    return true;
                default:
                    Toast.makeText(context, "Method not allowed on printer service.", Toast.LENGTH_LONG).show();
                    callbackContext.error("Method not allowed");
                    return false;
            }

        }else{
            callbackContext.error("PrinterService no available.");
            return false;
        }
    }

    private void connectPrinter(){
        this.context = this.cordova.getContext().getApplicationContext();
        Intent intent = new Intent();
        intent.setPackage("com.sunmi.extprinterservice");
        intent.setAction("com.sunmi.extprinterservice.PrinterService");
        this.context.bindService(intent, connService, Context.BIND_AUTO_CREATE);
    }

    private void disconnectPrinterService(){
        this.context.unbindService(connService);
        mPrinter = null;
    }

    private void printingService(JSONArray data) throws JSONException {
        for (int i = 0; i < data.length(); i++) {
            JSONObject aux = data.getJSONObject(i);
            switch (aux.getString("lineType")){
                case "text":
                    printTextLine(
                            aux.getString("data"),
                            aux.getInt("alignment"),
                            aux.getInt("size"),
                            aux.getBoolean("isBold")
                    );
                    break;
                case "qr":
                    printQRCode(
                            aux.getString("data"),
                            aux.getInt("alignment"),
                            aux.getInt("size")
                    );
                    break;
                case "barcode":
                    printBarcode(
                            aux.getString("data"),
                            aux.getBoolean("printLabel")
                    );
                    break;
                case "bitmap":
                    printImage(
                            aux.getString("data"),
                            aux.getInt("alignment")
                    );
                    break;
            }
        }
        endPrinting();
    }

    private boolean checkPrinterStatus(){
        try {
            if(mPrinter.getPrinterStatus() == 0){
                return true;
            }else{
                switch (mPrinter.getPrinterStatus()){
                    case -1:
                        Toast.makeText(context, "La impresora no se encuentra disponible, o el servicio de impresión no ha sido conectado.", Toast.LENGTH_LONG).show();
                        return false;
                    case 1:
                        Toast.makeText(context, "La impresora no está cerrada.", Toast.LENGTH_LONG).show();
                        return false;
                    case 2:
                        Toast.makeText(context, "La impresora no tiene papel.", Toast.LENGTH_LONG).show();
                        return false;
                    case 3:
                        Toast.makeText(context, "La impresora se sobrecalentó.", Toast.LENGTH_LONG).show();
                        return false;
                    default:
                        Toast.makeText(context, "La impresora tiene problemas. Contacte con suporte.", Toast.LENGTH_LONG).show();
                        return false;
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    /*
     * @description: Metodo para imprimir una linea. Si la linea es muy extensa, la impresora automáticamente la convierte en parrafo,
     *               y ajusta la impresión justificada.
     *
     * @param text: texto que se desea imprimir
     * @param textAlign: alineación del texto. 0:left, 1:center, 2:right,  otro:centro
     * @param textSize: tamaño del texto. Máx 5, Mín 1)
     * @param isBoldText: indica si el texto se imprime en negrita.
     * */
    private void printTextLine(String text,int textAlign, int textSize, boolean isBoldText){
        try {
            if(isBoldText){ mPrinter.sendRawData(boldOn()); }
            else { mPrinter.sendRawData(boldOff()); }

            if(textSize > 1){ mPrinter.setFontZoom(textSize-1, textSize); }
            else{ mPrinter.setFontZoom(1, 1); }

            mPrinter.setAlignMode(textAlign);
            mPrinter.printText(text);
            mPrinter.flush();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /*
     * @description: Recibe un texto y lo imprime como QR.
     *
     * @param data: texto que se desea convertir en QR. Máx 200 caracteres
     * @param size: tamaño del QR. Máx 16, Mín 2 (el tamaño se auto controla en función del largo del texto, para que pueda ser impreso.
     * @param alignment: alineación del QR en el voucher. 0:izquierda, 1:centro, 2:derecha
     * */
    private void printQRCode(String data,int alignment, int size ){
        try {
            if(data.length() > 200){
                return; // si se intenta imprimir más de 200 caracteres, se sale del proceso.
            }else if( data.length() <=200 && data.length() > 180){
                if(size > 7)
                    size = 7;
            }else if (data.length() <= 180 && data.length() > 140){
                if(size > 8)
                    size = 8;
            }else if (data.length() <= 140 && data.length() > 125){
                if(size > 9)
                    size = 9;
            }else if(data.length() <= 125 && data.length() > 90){
                if(size > 10)
                    size = 10;
            }else if(data.length() <=90 && data.length() > 70){
                if(size > 11)
                    size = 11;
            }else if(data.length() <= 70 && data.length() > 65){
                if(size > 12)
                    size = 12;
            }else if(data.length() <= 65 && data.length() > 50){
                if(size > 13)
                    size = 13;
            }else if(data.length() <= 50 && data.length() > 40){
                if(size > 15)
                    size = 15;
            }else if(data.length() < 40){
                if(size > 16)
                    size = 16;
            }
            mPrinter.setAlignMode(alignment);
            mPrinter.lineWrap(1);
            mPrinter.printQrCode(data, size, 1);
            mPrinter.lineWrap(1);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /*
     * @description: Proceso que imprime un codigo de barra de tipo CODE93. El barcode soporta caracteres A-Z, a-z, 0-9 y /, + , %, – , . , $
     *
     * @param data: información que se convertirá en barcode. Máx 13 caracteres.
     * @param printLabel: booleano que indica si se desea imprimir el texto "human friendly" bajo el código. true: imprime texto, false: solo imprime barcode, sin texto.
     * */
    private void printBarcode(String data, boolean printLabel){
        try {
            if(data.length() > 13){
                return;
            }
            mPrinter.setAlignMode(1);
            mPrinter.printBarCode(data, 7, 2, 150, (printLabel? 2 : 0));
            mPrinter.flush();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }

    /*
     * @description: método que imprime imagen png (sin fondo)
     *
     * @param base64image: base64 de imagen que se desea imprimir.
     * @param alignment: alineación de la imagen. 0:izquierda, 1:centro, 2:derecha
     * */
    private void printImage(String base64image, int alignment){
        try {
            //Se elimina la metadata del base64
            if(base64image.contains(",")){
                base64image = base64image.split(",")[1];
            }
            Bitmap image;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inTargetDensity = 160;
            options.inDensity = 160;
            byte[] decodedString = android.util.Base64.decode(base64image, Base64.DEFAULT);
            image = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            image = scaleImage(image);
            mPrinter.setAlignMode(alignment);
            mPrinter.printBitmap(image,0);
            mPrinter.flush();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /*
     * @description: Método para terminar impresión. Limpia el caché de la impresora, deja lineas al final del voucher, y corta el papel
     * */
    private void endPrinting(){
        try {
            mPrinter.lineWrap(2);
            mPrinter.flush();
            mPrinter.cutPaper(0,0);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private byte[] boldOn() {
        byte[] result = new byte[3];
        result[0] = 0x1B;
        result[1] = 69;
        result[2] = 0xF;
        return result;
    }

    private byte[] boldOff() {
        byte[] result = new byte[3];
        result[0] = 0x1B;
        result[1] = 69;
        result[2] = 0;
        return result;
    }

    private Bitmap scaleImage(Bitmap bitmap1) {
        int width = bitmap1.getWidth();
        int height = bitmap1.getHeight();
        int newWidth = (width/8+1)*8;
        float scaleWidth = ((float) newWidth) / width;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, 1);
        Bitmap newbm = Bitmap.createBitmap(bitmap1, 0, 0, width, height, matrix,true);
        return newbm;
    }
}
